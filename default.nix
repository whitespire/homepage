{ pkgs ? (
    let
      inherit (builtins) fetchTree fromJSON readFile;
      inherit ((fromJSON (readFile ./flake.lock)).nodes) nixpkgs gomod2nix;
    in
    import (fetchTree nixpkgs.locked) {
      overlays = [
        (import "${fetchTree gomod2nix.locked}/overlay.nix")
      ];
    }
  )
, buildGoApplication ? pkgs.buildGoApplication
}:

buildGoApplication {
  pname = "whitespire-homepage";
  version = "0.0.2";
  pwd = ./.;
  src = ./.;
  nativeBuildInputs = with pkgs; [go sass];
  buildInputs = with pkgs; [iproute];
  preBuildPhase = ''
  sass --update sass:static/css
  '';
  buildPhase = ''
  go build cmd/homepage/homepage.go
  '';
  installPhase = ''
    mkdir -p $out/bin
    cp homepage $out/bin
    cp -r static $out
    cp -r templates $out
  '';

  modules = ./gomod2nix.toml;
}
