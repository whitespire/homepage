package config

import (
	"fmt"
	"github.com/pelletier/go-toml/v2"
	"os"
)

type Config struct {
	Port     int16
	Services struct {
		Enabled        bool
		TitleDiscovery struct {
			Enabled   bool
			Exclusive bool
		}
	}
}

var Program Config

func parseConfig() {
	confDir, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}
	confFile, err := os.Open(fmt.Sprintf("%s/whitespire/homepage/config.toml", confDir))
	if err != nil {
		fmt.Printf("Error opening config file: %s", err)
		return
	}
	tomlDecoder := toml.NewDecoder(confFile)
	var fileContents Config
	err = tomlDecoder.Decode(&fileContents)
	if err != nil {
		fmt.Printf("Error deconding TOML: %s", err)
		return
	}
	Program = fileContents
}

func Init() {
	Program = Config{
		Port: 6001,
		Services: struct {
			Enabled        bool
			TitleDiscovery struct {
				Enabled   bool
				Exclusive bool
			}
		}{
			Enabled: true,
			TitleDiscovery: struct {
				Enabled   bool
				Exclusive bool
			}{
				Enabled:   true,
				Exclusive: true,
			},
		}}
}
