package types

type Set map[any]struct{}

func (set Set) Add(el any) {
	set[el] = struct{}{}
}

func (set Set) Remove(el any) {
	delete(set, el)
}

func (set Set) Has(el any) bool {
	_, ok := set[el]
	return ok
}

type PageData struct {
	Greeting string
	Services []Service
}

type Service struct {
	Name    string
	Address string
}
