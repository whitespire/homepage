package serverscan

import (
	"fmt"
	"github.com/patrickmn/go-cache"
	"gitlab.com/whitespire/homepage/internal/config"
	"gitlab.com/whitespire/homepage/internal/types"
	"golang.org/x/net/html"
	"log"
	"net/http"
	"os/exec"
	"regexp"
	"strings"
	"sync"
	"time"
)

func parseHtml(doc *html.Node) *string {
	for c := doc.FirstChild; c != nil; c = c.NextSibling {
		for a := c.FirstChild; a != nil; a = c.NextSibling {
			if a.Type == html.ElementNode && a.Data == "head" {
				for t := a.FirstChild; t != nil; t = t.NextSibling {
					if t.Type == html.ElementNode && t.Data == "title" {
						if t.FirstChild != nil && t.FirstChild.Type == html.TextNode {
							// if yes, retrieve FirstChild's data (name)
							name := t.FirstChild.Data
							return &name
						}
					}
				}
				return nil
			}
		}
	}
	return nil
}

var TitleCache = cache.New(5*time.Minute, 10*time.Minute)

func fetch(url string, result chan<- *string) {
	defer close(result)
	log.Printf("fetching %s", url)
	res, err := http.Get(fmt.Sprintf("http://%s/", url))
	if err != nil {
		return
	}
	log.Printf("fetch successful")
	doc, err := html.Parse(res.Body)
	if err != nil {
		panic(err)
	}
	name := parseHtml(doc)
	result <- name
}

func buildService(fields []string, result chan<- types.Service, wg *sync.WaitGroup) {
	defer func() {
		wg.Done()
	}()
	re := regexp.MustCompile(`".+"`)
	serviceName := re.FindString(fields[6])
	if serviceName == "\"homepage\"" {
		close(result)
		return
	}
	log.Printf("building %s\n", serviceName)
	address := fields[4]
	log.Println(config.Program.Services.TitleDiscovery.Enabled)
	if config.Program.Services.TitleDiscovery.Enabled {
		pageTitle, found := TitleCache.Get(fmt.Sprintf("%s%s", serviceName, address))
		if !found {
			ch := make(chan *string)
			var t *string
			go fetch(address, ch)

			select {
			case t = <-ch:
				break
			case <-time.After(time.Millisecond * 300):
				break
			}
			if t != nil {
				TitleCache.Set(fmt.Sprintf("%s%s", serviceName, address), *t, cache.DefaultExpiration)
				pageTitle, found = TitleCache.Get(fmt.Sprintf("%s%s", serviceName, address))
			}
			log.Println("cache MISS")
		} else {
			log.Println("cache HIT")
		}
		if pageTitle != nil {
			log.Println("Page titile present")

			serviceName = pageTitle.(string)
		} else {
			if config.Program.Services.TitleDiscovery.Exclusive {
				close(result)
				return
			}
		}
	}
	service := types.Service{
		Name:    serviceName,
		Address: address,
	}
	result <- service
	log.Println("finished building service.")
}

// getServices uses the "ss -tulnp" command to get applications listening on network ports.
func GetServices() (services []types.Service, err error) {
	var wg sync.WaitGroup
	log.Println("getServices()")
	cmd := exec.Command("ss", "-tulnp")
	output, err := cmd.Output()
	if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}
	lines := strings.Split(string(output), "\n")
	result := make(map[string]chan types.Service)
	for _, line := range lines[1:] {
		fields := strings.Fields(line)
		if len(fields) >= 7 {
			result[line] = make(chan types.Service, 1)
			wg.Add(1)
			go buildService(fields, result[line], &wg)
		}
	}
	log.Println("Finished assigning goroutines.")
	wg.Wait()
	log.Println("Finished executing goroutines.")
	for _, line := range lines[1:] {
		log.Println(line)
		if result[line] != nil {
			s, ok := <-result[line]
			if ok {
				log.Println("received ok from channel")
				services = append(services, s)
			}
		}

	}
	return services, nil
}
