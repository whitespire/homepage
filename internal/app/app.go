package app

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/whitespire/homepage/internal/config"
	"gitlab.com/whitespire/homepage/internal/serverscan"
	"gitlab.com/whitespire/homepage/internal/types"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("indexHandler()")
	execPath, err := os.Executable()
	if err != nil {
		panic(err)
	}
	execDir := filepath.Dir(execPath)
	templatesDir := filepath.Join(execDir, "..", "templates")
	tmpl := template.Must(template.ParseFiles(filepath.Join(templatesDir, "index.html")))
	services, err := serverscan.GetServices()
	if err != nil {
		panic(err)
	}
	data := types.PageData{
		Greeting: "^_^",
		Services: services,
	}
	tmpl.Execute(w, data)
}
func Run() {
	config.Init()
	execPath, err := os.Executable()
	if err != nil {
		panic(err)
	}
	execDir := filepath.Dir(execPath)
	staticDir := filepath.Join(execDir, "..", "static")
	fs := http.FileServer(http.Dir(staticDir))
	fmt.Printf("Created FileServer\n")
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.HandleFunc("/", indexHandler)
	port := config.Program.Port
	addr := fmt.Sprintf("127.0.0.1:%d", port)
	fmt.Printf("Listening on %s \n", addr)
	err = http.ListenAndServe(addr, nil)
	if err != nil {
		panic(err)
	}
}
