package main

import "gitlab.com/whitespire/homepage/internal/app"

func main() {
	app.Run()
}
