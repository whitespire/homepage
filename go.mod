module gitlab.com/whitespire/homepage

go 1.22.3

require (
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pelletier/go-toml/v2 v2.2.2
	golang.org/x/net v0.27.0
)
